var express = require('express');

var db = require('../utils/db');  //数据库管理工具
const route = express.Router();


route.get('/:userId/v2/',async function (req, res) {
    var userId = req.params.userId;
    var userInfo={};
    userInfo.trips = [];
    userInfo.userId=userId;
    userInfo.user_info={};
    var user=await db.getUserById(userId);
    userInfo.user_info.cover=user.cover;
    userInfo.user_info.name=user.nickName;
    userInfo.user_info.avatar_l=user.avatarUrl;
    userInfo.user_info.followers_count=await db.getFansCountById(userId);
    userInfo.user_info.followings_count=await db.getFollowerCountById(userId);

    db.getTripByUserId(userId,async function (data) {
        if(data==null){
            res.send(trips).end();
        }
        for(var index in data){
            var trip =data[index];
            trip.date_added=trip.create_time;
            trip.name=trip.title;
            trip.day_count=(trip.end_date - trip.start_date) / (24 * 60 * 60 * 1000) + 1;
            trip.total_comments_count=await db.getCommentsCountByTripId2(trip.id);
            trip.liked_count=await db.getEnshrineCountByTripId2(trip.id);
            userInfo.trips.push(trip);
        }
        res.send(userInfo).end();
    });

})
;

module.exports= route;